from PIL import Image # need this python library to get the image timestamp
import argparse
import os
import sys

parser = argparse.ArgumentParser()
parser.add_argument('--image-folder', default='/images/')
args = parser.parse_args()
print(args.image_folder)
imgFolder = args.image_folder
IMAGES_DB = "/imagesdata.csv"
files = []

try:
  truncateFile = open('%s%s' % (args.image_folder, IMAGES_DB), 'r+')
  truncateFile.truncate(0)
except IOError:
  print("File not found")

for r, d, f in os.walk(imgFolder):
    for file in f:
        if '.jpg' in file.lower() or '.jpeg' in file.lower() or '.gif' in file.lower() or '.png' in file.lower():
            files.append(os.path.join(r, file))

fdt = open('%s%s' % (args.image_folder, IMAGES_DB), 'w')
fdt.write('datetime,ampm,img\n')

for f in files:
    print(f)
    fnSplit = f.split('.')
    im = Image.open(f)
    mode = 'a' if os.path.exists('%s%s' % (args.image_folder, IMAGES_DB)) else 'w'
    fdt = open('%s%s' % (args.image_folder, IMAGES_DB), mode)
    exif = im._getexif()
    if exif != None:
      creation_time = exif.get(36867)
      print(creation_time)
      splitCreationTime = creation_time.split(':')
      AMPM = splitCreationTime[2].split(' ')
      print(AMPM)
      if int(AMPM[1]) > 12:
        ampmValue = "PM"
      else: 
        ampmValue = "AM"
      print(ampmValue)
      filenameOnly = f.split('/')
      print(len(filenameOnly))
      fdt.write(creation_time + "|%s|" % ampmValue + filenameOnly[len(filenameOnly)-1] + "\n")
      fdt.close()
    else: 
      print(f);
      os.remove(f);
      sys.exit("No Exif data available, kindly upload the original photo.");
