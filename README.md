#  Daily Temperature Telegram Bot
<img src="screen.png"/>

Click here to see the demo <a href="t.me/dailytempbot">The Daily temperature telegram bot</a>

##  Place all the images to the project/images directory

```
cp *.jpg /Users/phangty/Projects/dorscons_temperature_bot/images
```


## Get timestamp of the photo using python

Before running the server.js

```
conda install pillow
```

* Note : this python program will be invoke as sub process  when a new  photo is being uploaded via the web app.  In case any of the files are missing on the csv data file the devops engineer can run this script to update the data file manually via the shell.
```
python getImageCreatedDate.py --image-folder /Users/phangty/Projects/dorscons_temperature_bot/images
```

## Set all the env variables

```
TELEGRAM_TOKEN=<TELEGRAM BOT API TOKEN>
PORT=3000
IMG_DB_CSV=./images/imagesdata.csv
```

## Run the Upload Photo Web App
```
npm i nodemon -g
npm i
nodemon server.js
```
<br>
<img src="screen2.png"/>

## Run the NodeJS telegram bot 
```
npm i
nodemon botserver.js
```
### Bot father command list

```
temperature - send photo temp taken
takephoto - send room photo
onetime - onetime
custom - custom
special - special
pyramid - pyramid
italic - italic
random - random
caption - caption
simple - simple
inline - inline
mylocation - mylocation

```