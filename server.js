require('dotenv').config();
const express = require('express');
const bodyParser = require('body-parser');
const multer = require('multer');
const hbs = require('hbs');
const logger = require('morgan');
const { exec } = require('child_process');

var storage = multer.diskStorage({
    destination: function (req, file, cb) {
      cb(null, './images/')
    },
    filename: function (req, file, cb) {
      console.log(file);
      let fileExt = file.originalname.split('.');
      console.log(fileExt.length);
      const uniqueSuffix = Date.now() + '-' + Math.round(Math.random() * 1E9)
      cb(null, uniqueSuffix + '.'+ fileExt[fileExt.length-1])
    }
  })
  
const upload = multer({ storage: storage })

const app = express();
app.use(bodyParser());  // to use bodyParser (for text/number data transfer between clientg and server)
app.set('view engine', 'hbs');  // setting hbs as the view engine
app.set('views', __dirname + '/views');  // making ./views as the views directory
app.use(logger('dev'));  // Creating a logger (using morgan)
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(express.static(__dirname + '/images'));

const PORT = process.env.PORT;

app.get('/', (req, res) => {
    res.render('index.html');
});

app.post('/upload', upload.single('file-0a'), (req, res) => {
    if (req.file) {
        console.log('Uploading file...');
        var filename = req.file.filename;
        var uploadStatus = 'File Uploaded Successfully';
    } else {
        console.log('No File Uploaded');
        var filename = 'FILE NOT UPLOADED';
        var uploadStatus = 'File Upload Failed';
    }
    
    exec(`python ./getImageCreatedDate.py --image-folder ${process.env.IMG_DIRECTORY}`, (error, stdout, stderr) => {
        if (error) {
            console.error(`exec error: ${error}`);
            return res.render('error.hbs', {message: 'No EXIF info found in the photo.'});
        }
        console.log(`stdout: ${stdout}`);
        console.error(`stderr: ${stderr}`);
        return res.render('index.hbs', { status: uploadStatus, filename: `Filename: ${filename}` });
    }); 
});

app.listen(PORT,
    () => {
        console.info(`Application started on port ${PORT} at ${new Date()}`);
    }
)