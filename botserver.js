require('dotenv').config();
const Telegraf = require('telegraf');
      Extra = require('telegraf/extra'),
      csv = require('csv-parser'),
      fs = require('fs');

const Markup = require('telegraf/markup');
const { exec } = require('child_process');

const { reply } = Telegraf

const bot = new Telegraf(process.env.TELEGRAM_TOKEN);

const IMG_DB_CSV = process.env.IMG_DB_CSV;
const IMG_DIRECTORY = process.env.IMG_DIRECTORY;

bot.command('takephoto', (ctx) => {
    console.log('take photo');
    exec(`python ./takeme.py`, (error, stdout, stderr) => {
        if (error) {
            console.error(`exec error: ${error}`);
            return;
        }
        console.log(`stdout: ${stdout}`);
        console.error(`stderr: ${stderr}`);
        return ctx.replyWithPhoto({ source: `${IMG_DIRECTORY}/me.jpg` });
    });
});

bot.command('temperature', (ctx) => {
    console.log('get temperature');
    let foundFlag = false;
    let match = ctx.message.text;
    const message = match.split(' ')[1];
    const day = match.split(' ')[2];
    let results = [];
    fs.createReadStream(IMG_DB_CSV)
        .pipe(csv({ separator: '|' }))
        .on('data', (data) => results.push(data))
        .on('end', () => {
            for(let i = 0; i < results.length; i++){
                if(results[i]['0'].includes(message)){
                    if(results[i]['1'] == day){
                        foundFlag = true;
                        let finalImagedir = `${results[i]['2']}`;
                        if(process.platform === 'darwin'){
                          finalImagedir = `${IMG_DIRECTORY}/${results[i]['2']}`
                        }
                        console.log(finalImagedir);
                        return ctx.replyWithPhoto({ source: finalImagedir })
                    }
                }
            }
            console.log(foundFlag);
            if(!foundFlag){
                return ctx.reply('Ooppss ! No photo taken ?');
            }
        });
});

bot.command('onetime', ({ reply }) =>
  reply('One time keyboard', Markup
    .keyboard(['/simple', '/inline', '/pyramid'])
    .oneTime()
    .resize()
    .extra()
  )
)

bot.hears('🔍 Search', ctx => ctx.reply('Yay!'))
bot.hears('📢 Ads', ctx => ctx.reply('Free hugs. Call now!'))

bot.command('custom', ({ reply }) => {
  return reply('Custom buttons keyboard', Markup
    .keyboard([
      ['🔍 Search', '😎 Popular'], // Row1 with 2 buttons
      ['☸ Setting', '📞 Feedback'], // Row2 with 2 buttons
      ['📢 Ads', '⭐️ Rate us', '👥 Share'] // Row3 with 3 buttons
    ])
    .oneTime()
    .resize()
    .extra()
  )
})
  

bot.command('special', (ctx) => {
  return ctx.reply('Special buttons keyboard', Extra.markup((markup) => {
    return markup.resize()
      .keyboard([
        markup.contactRequestButton('Send contact'),
        markup.locationRequestButton('Send location')
      ])
  }))
})

bot.command('pyramid', (ctx) => {
  return ctx.reply('Keyboard wrap', Extra.markup(
    Markup.keyboard(['one', 'two', 'three', 'four', 'five', 'six'], {
      wrap: (btn, index, currentRow) => currentRow.length >= (index + 1) / 2
    })
  ))
})

bot.command('simple', (ctx) => {
  return ctx.replyWithHTML('<b>Coke</b> or <i>Pepsi?</i>', Extra.markup(
    Markup.keyboard(['Coke', 'Pepsi'])
  ))
})

bot.command('start', (ctx)=>{
  return ctx.reply('Hi How are you today ? 👍 ?');
})

bot.command('inline', (ctx) => {
  return ctx.reply('<b>Coke</b> or <i>Pepsi?</i>', Extra.HTML().markup((m) =>
    m.inlineKeyboard([
      m.callbackButton('Coke', 'Coke'),
      m.callbackButton('Pepsi', 'Pepsi')
    ])))
})

bot.command('random', (ctx) => {
  return ctx.reply('random example',
    Markup.inlineKeyboard([
      Markup.callbackButton('Coke', 'Coke'),
      Markup.callbackButton('Dr Pepper', 'Dr Pepper', Math.random() > 0.5),
      Markup.callbackButton('Pepsi', 'Pepsi')
    ]).extra()
  )
})

bot.command('caption', (ctx) => {
  return ctx.replyWithPhoto({ url: 'https://picsum.photos/200/300/?random' },
    Extra.load({ caption: 'What do you think about this photo?' })
      .markdown()
      .markup((m) =>
        m.inlineKeyboard([
          m.callbackButton('Plain', 'plain'),
          m.callbackButton('Italic', 'italic')
        ])
      )
  )
})

bot.hears(/\/wrap (\d+)/, (ctx) => {
  return ctx.reply('Keyboard wrap', Extra.markup(
    Markup.keyboard(['one', 'two', 'three', 'four', 'five', 'six'], {
      columns: parseInt(ctx.match[1])
    })
  ))
})

bot.action('Dr Pepper', (ctx, next) => {
  return ctx.reply('👍').then(() => next())
})

bot.action('plain', async (ctx) => {
  await ctx.answerCbQuery()
  await ctx.editMessageCaption('Caption', Markup.inlineKeyboard([
    Markup.callbackButton('Plain', 'plain'),
    Markup.callbackButton('Italic', 'italic')
  ]))
})

bot.action('italic', async (ctx) => {
  await ctx.answerCbQuery()
  await ctx.editMessageCaption('_Caption_', Extra.markdown().markup(Markup.inlineKeyboard([
    Markup.callbackButton('Plain', 'plain'),
    Markup.callbackButton('* Italic *', 'italic')
  ])))
})

bot.action(/.+/, (ctx) => {
  return ctx.answerCbQuery(`Oh, ${ctx.match[0]}! Great choice`)
})

bot.launch()
